

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $title; ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="./bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="./dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="./dist/css/skins/skin-black.min.css">
  
  <link rel="stylesheet" href="./bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="./plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <link rel="stylesheet" href="./dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="./dist/css/skins/_all-skins.min.css">

</head>
<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>UTS</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Dashboard</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-default">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="<?php if($currentPage == 'JB' || $currentPage == 'Kogan' || $currentPage == 'Charts'){echo 'active';}?> treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>CEO</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
              <li class="<?php if($currentPage == 'JB'){echo 'active';}?>"><a href="../index.php"><i class="fa fa-dashboard"></i> JB HiFi</a></li>
              <li class="<?php if($currentPage == 'Kogan'){echo 'active';}?>"><a href="./koganDashboard.php"><i class="fa fa-dashboard"></i>Kogan</a></li>
              <li class="<?php if($currentPage == 'Charts'){echo 'active';}?>"><a href="./charts.php"><i class="fa fa-pie-chart"></i> Comparison</a></li>
          </ul>
        </li>
        <li  class="treeview <?php if($currentPage =='Tests'){echo 'active';}?>">
            <a href="./tests.php">
            <i class="fa fa-cogs"></i>
            <span>Tests</span>
          </a>
        </li>
        
        <li  style="margin-top: 100%" class="treeview <?php if($currentPage =='About'){echo 'active';}?>">
            <a href="./about.php">
            <i class="fa fa-info"></i>
            <span>About US</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
