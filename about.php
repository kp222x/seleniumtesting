<?php
$title = "About Us";
$currentPage = "About";
include './template.php';
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content">
      <div class="row">
          <div class="col-md-4">
              <div class="panel panel-primary">
                  <div class="panel-heading text-center"><h3>Technologies we have used</h3></div>
                  <div class="panel-body " style="font-size: medium">
                    <dl>
                      <dt>Fron End</dt>
                      <dd>
                            <ul class="list-unstyled">
                                <ul>
                                  <li>HTML5</li>
                                  <li>CSS3</li>
                                  <li>Java Script</li>
                                  <li>Twitter Bootstap</li>
                                  <li>JQuery</li>
                                  <li>Chart Js</li>
                                </ul>
                            </ul>
                      </dd>
                      <dt>Back End</dt>
                      <dd>
                            <ul class="list-unstyled">
                                <ul>
                                  <li>Java</li>
                                  <li>Php</li>
                                  <li>Selenium</li>
                                  <li>Web Services</li>
                                </ul>
                            </ul>
                      </dd>
                      <dt>Database</dt>
                      <dd>
                            <ul class="list-unstyled">
                                <ul>
                                  <li>mssql</li>
                                </ul>
                            </ul>
                      </dd>
                      <dt>Servers</dt>
                      <dd>
                            <ul class="list-unstyled">
                                <ul>
                                  <li>UTS server</li>
                                  <li>Azure server</li>
                                </ul>
                            </ul>
                      </dd>
                      <dt>Revision Control System</dt>
                      <dd>
                            <ul class="list-unstyled">
                                <ul>
                                  <li>BitBucket</li>
                                </ul>
                            </ul>
                      </dd>
                    </dl>
                </div>
              </div>
          </div>
          <div class="col-md-4">
              <div class="panel panel-primary">
                  <div class="panel-heading text-center"><h3>Tools we have used</h3></div>
                  <div class="panel-body" style="font-size: medium">
                    <dl>
                      <dt>Pingdom</dt>
                      <dd> Used for Response time of webpages
                            <ul class="list-unstyled">
                                <ul>
                                    <li><a>https://tools.pingdom.com/</a></li>
                                </ul>
                            </ul>
                      </dd>
                      <dt>Sem Rush</dt>
                      <dd>  Used To get Site Details Like Orgainc Traffic and Most searched Product
                            <ul class="list-unstyled">
                                <ul>
                                    <li><a>https://www.semrush.com/</a></li>
                                </ul>
                            </ul>
                      </dd>
                      <dt>Alexa</dt>
                      <dd>  Used To get Ranking and Bounce Rate of a website
                            <ul class="list-unstyled">
                                <ul>
                                    <li><a>http://www.alexa.com/siteinfo</a></li>
                                </ul>
                            </ul>
                      </dd>
                      <dt>Google Insights</dt>
                      <dd>  Used To get performance of a website
                            <ul class="list-unstyled">
                                <ul>
                                    <li><a>https://developers.google.com/</a></li>
                                </ul>
                            </ul>
                      </dd>
                    </dl>
                </div>
              </div>
          </div>
          <div class="col-md-4">
              <div class="panel panel-primary">
                  <div class="panel-heading text-center"><h3>Demo</h3></div>
                  <div class="panel-body" style="font-size: medium">
                      <video class="video_player" id="player" width="100%" controls="controls" autoplay="autoplay" loop="loop">
                          <source src="./dist/video/Demo.mp4"/>
                        Your browser does not support the HTML5 video tag.  Use a better browser!
                      </video>
                </div>
              </div>
          </div>
      </div>
        
        <div class="row" style="margin-bottom: 100px;">
            <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-info">
                  <div class="panel-heading text-center"><h3>How it Works?</h3></div>
                  <div class="panel-body text-center" style="font-size: large">
                      We have created Java programs for Testings with selenium. <br>
                      This programs are saved as a JAR files. We've created a webservice to connect test output to the server. <br>
                      Programs are scheduled to run on particular times with Windows Scheduler.<br>
                      After each execution of the test webservice will be called and Result will be pushed to the Azure server.<br>
                </div>
            </div>
        </div>
      <!-- /.row -->
        </div>
        
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-info">
                  <div class="panel-heading text-center"><h3>Who we are?</h3></div>
                  <div class="panel-body text-center" style="font-size: large">
                      <b>We are Software Test Beginners Group for Enterprise Software Testing Autumn 2017.</b><br>
                    <br>
                    Padam Rao | Karan Parikh | Shradhey Trivedi | Amiteshwar Singh 
                    Navjot Singh | George Jojo
                </div>
            </div>
        </div>
      <!-- /.row -->
        </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
    </div>
    <strong>Copyright &copy; 2017 UTS</a>.</strong> All rights
    reserved.
  </footer>
  </aside>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="./plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="./bootstrap/js/bootstrap.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="./plugins/chartjs/Chart.min.js"></script>
<!-- FastClick -->
<script src="./plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="./dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="./dist/js/demo.js"></script>
<!-- page script -->
<script type="text/javascript">
function goFullscreen(id) {
  var element = document.getElementById(id);       
  if (element.mozRequestFullScreen) {
    element.mozRequestFullScreen();
  } else if (element.webkitRequestFullScreen) {
    element.webkitRequestFullScreen();
  }  
}
</script>
</body>
</html>
