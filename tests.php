<?php
$title = "Test Comparison";
$currentPage = "Tests";
include './template.php';
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-6">
                <h3 style="text-align: center">JB HiFI</h3>
            </div>
            <div class="col-md-6">
                <h3 style="text-align: center">Kogan</h3>
            </div>
        </div>
        
      <div class="row">
          <div class="col-md-12">
              <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-12">
                              <div class="alert alert-success alert-dismissible">
                                  <p style="font-size: large; font-weight: bold">Usability Testing</p>
                              </div>
                              <div class=" col-md-11 col-md-offset-1 alert alert-success alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-check"></i>No spelling or grammatical errors</p>
                              </div>
                            <div class=" col-md-11  col-md-offset-1 alert alert-success alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-check"></i>All fonts are uniform</p>
                              </div>
                            <div class=" col-md-11  col-md-offset-1 alert alert-success alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-check"></i>All the text should be properly aligned</p>
                              </div>
                            <div class=" col-md-11  col-md-offset-1 alert alert-success alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-check"></i>All the error messages should be correct</p>
                              </div>
                            <div class=" col-md-11  col-md-offset-1 alert alert-success alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-check"></i>All the fields should be properly aligned</p>
                              </div>
                            <div class=" col-md-11  col-md-offset-1 alert alert-success alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-check"></i>Home link should be there on every single page</p>
                              </div>
                            <div class=" col-md-11  col-md-offset-1 alert alert-success alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-check"></i>User friendly</p>
                              </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                              <div class="alert alert-success alert-dismissible">
                                  <p style="font-size: large; font-weight: bold">Functional Testing</p>
                              </div>
                            <div class=" col-md-11 col-md-offset-1 alert alert-success alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-check"></i>All the mandatory fields should be validated</p>
                              </div>
                            <div class=" col-md-11 col-md-offset-1 alert alert-success alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-check"></i>The asterisk sign should display for all the mandatory fields</p>
                              </div>
                            <div class=" col-md-11 col-md-offset-1 alert alert-success alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-check"></i>The system should not display the error message for optional fields</p>
                              </div>
                            <div class=" col-md-11 col-md-offset-1 alert alert-success alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-check"></i>The numeric fields should not accept the alphabets</p>
                              </div>
                            <div class=" col-md-11 col-md-offset-1 alert alert-success alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-check"></i>Negative numbers if allowed for numeric fields</p>
                              </div>
                            <div class=" col-md-11 col-md-offset-1 alert alert-success alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-check"></i>The max length of every field to ensure the data is not truncated</p>
                              </div>
                        </div>
                    </div>
                  <br>
                  <br>
               
                    <div class="row">
                        <div class="col-md-12">
                              <div class="alert alert-warning alert-dismissible">
                                  <p style="font-size: large; font-weight: bold">Compatibility Testing</p>
                              </div>
                            <div class=" col-md-11 col-md-offset-1 alert alert-success alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-check"></i>Website run in different browsers</p>
                              </div>
                            <div class=" col-md-11 col-md-offset-1 alert alert-success alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-check"></i>The images display correctly in different browsers</p>
                              </div>
                            <div class=" col-md-11 col-md-offset-1 alert alert-success alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-check"></i>Test the fonts are usable in different browsers</p>
                              </div>
                            <div class=" col-md-11 col-md-offset-1 alert alert-success alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-check"></i>Test the java script code is usable in different browsers</p>
                              </div>
                            <div class=" col-md-11 col-md-offset-1 alert alert-warning alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-warning"></i>Mobile and desktop compatibility</p>
                                    Poor Compatibilty with mobile
                              </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                              <div class="alert alert-success alert-dismissible">
                                  <p style="font-size: large; font-weight: bold">Security Testing</p>
                              </div>
                            <div class=" col-md-11 col-md-offset-1 alert alert-success alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-check"></i>HTTPS (SSL)</p>
                              </div>
                            <div class=" col-md-11 col-md-offset-1 alert alert-success alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-check"></i>Private information should display in encrypted format</p>
                              </div>
                            <div class=" col-md-11 col-md-offset-1 alert alert-success alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-check"></i>Password rules are implemented on all authentication pages</p>
                              </div>
                            <div class=" col-md-11 col-md-offset-1 alert alert-success alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-check"></i>Password is change is working</p>
                              </div>
                            <div class=" col-md-11 col-md-offset-1 alert alert-success alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-check"></i>The error messages should not display any important information</p>
                              </div>
                        </div>
                    </div>
                  <div class="row">
                        <div class="col-md-12">
                              <div class="alert alert-info alert-dismissible">
                                  <p style="font-size: large; font-weight: bold">Performance</p>
                              </div>
                            <div class=" col-md-11 col-md-offset-1 alert alert-info alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-info"></i>Website: Max 6.9s</p>
                                    <p style="font-size: large;"><i class="icon fa fa-info"></i>First Page: Max 3.97s</p>
                              </div>
                        </div>
                    </div>
                  <div class="row">
                        <div class="col-md-12">
                              <div class="alert alert-info alert-dismissible">
                                  <p style="font-size: large; font-weight: bold">Boundary Value</p>
                              </div>
                            <div class=" col-md-11 col-md-offset-1 alert alert-info alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-info"></i>Max 100 Products in Cart</p>
                              </div>
                        </div>
                    </div>
              </div>
              
              <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-12">
                              <div class="alert alert-success alert-dismissible">
                                  <p style="font-size: large; font-weight: bold">Usability Testing</p>
                              </div>
                              <div class=" col-md-11 col-md-offset-1 alert alert-success alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-check"></i>No spelling or grammatical errors</p>
                              </div>
                            <div class=" col-md-11  col-md-offset-1 alert alert-success alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-check"></i>All fonts are uniform</p>
                              </div>
                            <div class=" col-md-11  col-md-offset-1 alert alert-success alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-check"></i>All the text should be properly aligned</p>
                              </div>
                            <div class=" col-md-11  col-md-offset-1 alert alert-success alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-check"></i>All the error messages should be correct</p>
                              </div>
                            <div class=" col-md-11  col-md-offset-1 alert alert-success alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-check"></i>All the fields should be properly aligned</p>
                              </div>
                            <div class=" col-md-11  col-md-offset-1 alert alert-success alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-check"></i>Home link should be there on every single page</p>
                              </div>
                            <div class=" col-md-11  col-md-offset-1 alert alert-success alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-check"></i>User friendly</p>
                              </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                              <div class="alert alert-warning alert-dismissible">
                                  <p style="font-size: large; font-weight: bold">Functional Testing</p>
                              </div>
                            <div class=" col-md-11 col-md-offset-1 alert alert-warning alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-warning"></i>All the mandatory fields should be validated</p>
                                    No verification for Address
                              </div>
                            <div class=" col-md-11 col-md-offset-1 alert alert-warning alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-warning"></i>The asterisk sign should display for all the mandatory fields</p>
                                    No indication of mandatory fields 
                            </div>
                            <div class=" col-md-11 col-md-offset-1 alert alert-success alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-check"></i>The system should not display the error message for optional fields</p>
                              </div>
                            <div class=" col-md-11 col-md-offset-1 alert alert-success alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-check"></i>The numeric fields should not accept the alphabets</p>
                              </div>
                            <div class=" col-md-11 col-md-offset-1 alert alert-success alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-check"></i>Negative numbers if allowed for numeric fields</p>
                              </div>
                            <div class=" col-md-11 col-md-offset-1 alert alert-success alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-check"></i>The max length of every field to ensure the data is not truncated</p>
                              </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                              <div class="alert alert-warning alert-dismissible">
                                  <p style="font-size: large; font-weight: bold">Compatibility Testing</p>
                              </div>
                            <div class=" col-md-11 col-md-offset-1 alert alert-success alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-check"></i>Website run in different browsers</p>
                              </div>
                            <div class=" col-md-11 col-md-offset-1 alert alert-success alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-check"></i>The images display correctly in different browsers</p>
                              </div>
                            <div class=" col-md-11 col-md-offset-1 alert alert-success alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-check"></i>Test the fonts are usable in different browsers</p>
                              </div>
                            <div class=" col-md-11 col-md-offset-1 alert alert-success alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-check"></i>Test the java script code is usable in different browsers</p>
                              </div>
                            <div class=" col-md-11 col-md-offset-1 alert alert-warning alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-warning"></i>Mobile and desktop compatibility</p>
                                    Poor Compatibilty with mobile
                              </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                              <div class="alert alert-success alert-dismissible">
                                  <p style="font-size: large; font-weight: bold">Security Testing</p>
                              </div>
                            <div class=" col-md-11 col-md-offset-1 alert alert-success alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-check"></i>HTTPS (SSL)</p>
                              </div>
                            <div class=" col-md-11 col-md-offset-1 alert alert-success alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-check"></i>Private information should display in encrypted format</p>
                              </div>
                            <div class=" col-md-11 col-md-offset-1 alert alert-success alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-check"></i>Password rules are implemented on all authentication pages</p>
                              </div>
                            <div class=" col-md-11 col-md-offset-1 alert alert-success alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-check"></i>Password is change is working</p>
                              </div>
                            <div class=" col-md-11 col-md-offset-1 alert alert-success alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-check"></i>The error messages should not display any important information</p>
                              </div>
                        </div>
                    </div> 
                  <div class="row">
                        <div class="col-md-12">
                              <div class="alert alert-info alert-dismissible">
                                  <p style="font-size: large; font-weight: bold">Performance</p>
                              </div>
                            <div class=" col-md-11 col-md-offset-1 alert alert-info alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-info"></i>Website: Max 3.32s</p>
                                    <p style="font-size: large;"><i class="icon fa fa-info"></i>First Page: Avg 2.76s</p>                                        
                            </div>
                        </div>
                    </div>
                  <div class="row">
                        <div class="col-md-12">
                              <div class="alert alert-info alert-dismissible">
                                  <p style="font-size: large; font-weight: bold">Boundary Value</p>
                              </div>
                            <div class=" col-md-11 col-md-offset-1 alert alert-info alert-dismissible">
                                    <p style="font-size: large;"><i class="icon fa fa-info"></i>Max 7 Products in Cart</p>
                              </div>
                        </div>
                    </div>
              </div>
          </div>    
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
    <section class="content-header">
        <h1 style="text-align: center">
        Technologies Used
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
            <div class="col-md-6">
                <h4 class="text-center">JB HiFi</h4>
                <img src="./dist/img/JB_Tech.PNG" class="img-responsive img-rounded" alt="JB HiFi Tech"/>
            </div>
            <div class="col-md-6">
                <h4 class="text-center">Kogan</h4>
                <img src="./dist/img/Kogan_Tech.PNG" class="img-responsive img-rounded" alt="Kogan Tech"/>
            </div>
            </div>
        </div>
    </section>
    
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
    </div>
    <strong>Copyright &copy; 2017 UTS</a>.</strong> All rights
    reserved.
  </footer>
  </aside>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="./plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="./bootstrap/js/bootstrap.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="./plugins/chartjs/Chart.min.js"></script>
<!-- FastClick -->
<script src="./plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="./dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="./dist/js/demo.js"></script>
<!-- page script -->

</body>
</html>
