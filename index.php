<?php
$title = "JB HiFi Dashboard";
$currentPage = "JB";
include './template.php';
?>
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        JB HiFi
      </h1>
    </section>

<!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-star"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Rank in Aus</span>
              <span class="info-box-number">82</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
       <!-- ./col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <!-- small box -->
          <div class="info-box">
             <span class="info-box-icon bg-green"><i class="fa fa-hourglass-2"></i></span>
            
            <div class="info-box-content">
              <span class="info-box-text">Response Time</span>
              <span class="info-box-number">3.97<small>s</small></span>
            </div>
          </div>
        </div>
        <!-- ./col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-search"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Organic Traffic</span>
              <span class="info-box-number">3.8<small>M</small> <small>Traffic</small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-industry"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Bounce Rate</span>
              <span class="info-box-number">41<small>%</small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <div class="row">
        <div class="col-md-6  col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-google"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Google INSIGHTS-Desktop</span>
              <span class="info-box-number">Poor <small>42/100</small></span>
              <span class="info-box-number"><small>Page is not optimized and is likely to deliver a slow user experience</small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
       <!-- ./col -->
        <div class="col-md-6 col-sm-6 col-xs-12">
          <!-- small box -->
          <div class="info-box">
             <span class="info-box-icon bg-green"><i class="fa fa-google"></i></span>
            
            <div class="info-box-content">
              <span class="info-box-text">Google INSIGHTS-Mobile</span>
              <span class="info-box-number">Poor <small>43/100</small></span>
              <span class="info-box-number"><small>Page is not optimized and is likely to deliver a slow user experience</small></span>
            </div>
          </div>
        </div>
        <!-- ./col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>
        <!-- /.col -->
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Weekly Response Time: www.jbhifi.com.au</h3>

            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-8">
                  <p class="text-center">
                    <strong>Response: 24 May, 2017 - 30 May, 2017</strong>
                  </p>

                  <div class="chart">
                    <!-- Sales Chart Canvas -->
                    <canvas id="salesChart" style="height: 180px;"></canvas>
                  </div>
                  <!-- /.chart-responsive -->
                </div>
                
                <div class="col-md-4">
                  <p class="text-center">
                    <strong>Time</strong>
                  </p>

                  <div class="progress-group">
                    <span class="progress-text">Max Response Time</span>
                    <span class="progress-number"><b>3.97s</b></span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-green" style="width: 100%"></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
                  <div class="progress-group">
                    <span class="progress-text">Min Response Time</span>
                    <span class="progress-number"><b>2.21s</b></span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-green" style="width: 55.66%"></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
                  <div class="progress-group">
                    <span class="progress-text">Average Response Time</span>
                    <span class="progress-number"><b>2.53s</b></span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-green" style="width: 63.72%"></div>
                    </div>
                  </div>
                  
                </div>
                <!-- /.col -->
              <!-- /.row -->
            </div>

            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
          <!-- /.row -->
            </div>
            <!-- /.box-body -->
            
            <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Weekly Response Time: Average Response of Website</h3>

            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-8">
                  <p class="text-center">
                    <strong>Response: 24 May, 2017 - 30 May, 2017</strong>
                  </p>

                  <div class="chart">
                    <!-- Sales Chart Canvas -->
                    <canvas id="salesChartJb" style="height: 180px;"></canvas>
                  </div>
                  <!-- /.chart-responsive -->
                </div>
                
                <div class="col-md-4">
                  <p class="text-center">
                    <strong>Time</strong>
                  </p>

                  <div class="progress-group">
                    <span class="progress-text">Max Response Time</span>
                    <span class="progress-number"><b>6.9s</b></span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-green" style="width: 100%"></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
                  <div class="progress-group">
                    <span class="progress-text">Min Response Time</span>
                    <span class="progress-number"><b>2.38s</b></span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-green" style="width: 34.49%"></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
                  <div class="progress-group">
                    <span class="progress-text">Average Response Time</span>
                    <span class="progress-number"><b>3.1s</b></span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-green" style="width: 44.9%"></div>
                    </div>
                  </div>
                  
                </div>
                <!-- /.col -->
              <!-- /.row -->
            </div>

            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
          <!-- /.row -->
            </div>
            <!-- /.box-body -->
            
</div>


  <!-- /.content-wrapper -->
</section>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
    </div>
    <strong>Copyright &copy; 2017 UTS</a>.</strong> All rights
    reserved.
  </footer>
  
<!-- ./wrapper -->

<script src="./plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="./bootstrap/js/bootstrap.min.js"></script>
<script src="./plugins/fastclick/fastclick.js"></script>
<script src="./dist/js/app.min.js"></script>
<script src="./plugins/sparkline/jquery.sparkline.min.js"></script>
<script src="./plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="./plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="./plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="./plugins/chartjs/Chart.min.js"></script>
<script src="./dist/js/pages/dashboard2.js"></script>
<script src="./dist/js/demo.js"></script>
</body>
</html>
