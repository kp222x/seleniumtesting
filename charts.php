<?php
$title = "Comparison";
$currentPage = "Charts";
include './template.php';
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 style="text-align: center">
        JB HiFi vs Kogan
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-6">
                <h2 style="text-align: center">JB HiFI</h2>
            </div>
            <div class="col-md-6">
                <h2 style="text-align: center">Kogan</h2>
            </div>
        </div>
        
     <div class="row">
         <div class="col-md-3">
           <div class="info-box pull-right">
            <span class="info-box-icon bg-green"><i class="fa fa-star"></i></span>

            <div class="info-box-content">
                <span class="info-box-text" style="font-size: larger">Rank in Aus</span>
              <span class="info-box-number">82</span>
            </div>
            <!-- /.info-box-content -->
          </div>
         </div>
         <div class="col-md-3">
           <div class="info-box pull-right">
            <span class="info-box-icon bg-green"><i class="fa fa-search"></i></span>

            <div class="info-box-content">
              <span class="info-box-text" style="font-size: larger">Organic Traffic</span>
              <span class="info-box-number">3.8<small>M</small> <small>Traffic</small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
         </div>
         
         <div class="col-md-3">
           <div class="info-box pull-right">
            <span class="info-box-icon bg-aqua"><i class="fa fa-star"></i></span>

            <div class="info-box-content">
              <span class="info-box-text" style="font-size: larger">Rank in Aus</span>
              <span class="info-box-number">196</span>
            </div>
            <!-- /.info-box-content -->
          </div>
         </div>
         <div class="col-md-3">
           <div class="info-box pull-right">
            <span class="info-box-icon bg-aqua"><i class="fa fa-search"></i></span>

            <div class="info-box-content">
              <span class="info-box-text" style="font-size: larger">Organic Traffic</span>
              <span class="info-box-number">586<small>K</small> <small>Traffic</small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
         </div>
     </div>
        
        
      <div class="row">
          <div class="col-md-12">
              <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-12">
                              <div class="alert alert-success alert-dismissible">
                                  <h4><i class="icon fa fa-check"></i>Usability Testing</h4>
                                  Success. Passed All Tests.
                              </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                              <div class="alert alert-success alert-dismissible">
                                  <h4><i class="icon fa fa-check"></i>Functional Testing</h4>
                                  Success. Passed All Tests.
                              </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                              <div class="alert alert-warning alert-dismissible">
                                  <h4><i class="icon fa fa-warning"></i> Compatibility Testing</h4>
                                  1 Test Failed!
                              </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                              <div class="alert alert-success alert-dismissible">
                                  <h4><i class="icon fa fa-check"></i>Security Testing</h4>
                                  Success. Passed All Tests.
                              </div>
                        </div>
                    </div> 
              </div>
              
              <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-12">
                              <div class="alert alert-success alert-dismissible">
                                  <h4><i class="icon fa fa-check"></i>Usability Testing</h4>
                                  Success. Passed All Tests.
                              </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                              <div class="alert alert-warning alert-dismissible">
                                  <h4><i class="icon fa fa-warning"></i>Functional Testing</h4>
                                  2 Tests Failed!
                              </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                              <div class="alert alert-warning alert-dismissible">
                                  <h4><i class="icon fa fa-warning"></i> Compatibility Testing</h4>
                                  1 Test Failed!
                              </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                              <div class="alert alert-success alert-dismissible">
                                  <h4><i class="icon fa fa-check"></i>Security Testing</h4>
                                  Success. Passed All Tests.
                              </div>
                        </div>
                    </div> 
              </div>
          </div>    
      </div>
        
        
      <div class="row">
        <div class="col-md-6">
          <!-- AREA CHART -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Weekly Response Rate: Average Response of Website</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="chart">
                <canvas id="areaChart" style="height:250px"></canvas>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          
        </div>
        <!-- /.col (LEFT) -->
        <div class="col-md-6">
         <!-- AREA CHART -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Weekly Response Rate: Average Response of Website</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="chart">
                <canvas id="areaChart2" style="height:250px"></canvas>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col (RIGHT) -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
    </div>
    <strong>Copyright &copy; 2017 UTS</a>.</strong> All rights
    reserved.
  </footer>
  </aside>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="./plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="./bootstrap/js/bootstrap.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="./plugins/chartjs/Chart.min.js"></script>
<!-- FastClick -->
<script src="./plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="./dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="./dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    /* ChartJS
     * -------
     * Here we will create a few charts using ChartJS
     */

    //--------------
    //- AREA CHART -
    //--------------

    // Get context with jQuery - using jQuery's .get() method.
    var areaChartCanvas = $("#areaChart").get(0).getContext("2d");
    // This will get the first returned node in the jQuery collection.
    var areaChart = new Chart(areaChartCanvas);

    var areaChartData = {
    labels: ["24 May", "25 May","26 May","27 May","28 May","29 May", "30 May"],
      datasets: [
        {
          label: "Time",
          fillColor: "rgba(0,255,0,0.5)",
          strokeColor: "rgba(60,141,188,0.8)",
          pointColor: "#3b8bba",
          pointStrokeColor: "rgba(0,0,0,1)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(60,141,188,1)",
          data : [6.9 , 4.1 , 3.2 , 3.24, 2.38 , 3.2 , 3.16]
        }
      ]
    };

    var areaChartOptions = {
      //Boolean - If we should show the scale at all
      showScale: true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines: false,
      //String - Colour of the grid lines
      scaleGridLineColor: "rgba(0,0,0,.05)",
      //Number - Width of the grid lines
      scaleGridLineWidth: 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines: true,
      //Boolean - Whether the line is curved between points
      bezierCurve: true,
      //Number - Tension of the bezier curve between points
      bezierCurveTension: 0.3,
      //Boolean - Whether to show a dot for each point
      pointDot: false,
      //Number - Radius of each point dot in pixels
      pointDotRadius: 4,
      //Number - Pixel width of point dot stroke
      pointDotStrokeWidth: 1,
      //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
      pointHitDetectionRadius: 20,
      //Boolean - Whether to show a stroke for datasets
      datasetStroke: true,
      //Number - Pixel width of dataset stroke
      datasetStrokeWidth: 2,
      //Boolean - Whether to fill the dataset with a color
      datasetFill: true,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
      //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio: true,
      //Boolean - whether to make the chart responsive to window resizing
      responsive: true
    };

    //Create the line chart
    areaChart.Line(areaChartData, areaChartOptions);

 //--------------
    //- AREA CHART -
    //--------------

    // Get context with jQuery - using jQuery's .get() method.
    var areaChartCanvas = $("#areaChart2").get(0).getContext("2d");
    // This will get the first returned node in the jQuery collection.
    var areaChart2 = new Chart(areaChartCanvas);

    var areaChartData = {
    labels: ["24 May", "25 May","26 May","27 May","28 May","29 May", "30 May"],
      datasets: [
        {
          label: "Digital Goods",
          fillColor: "rgba(60,141,188,0.9)",
          strokeColor: "rgba(60,141,188,0.8)",
          pointColor: "#3b8bba",
          pointStrokeColor: "rgba(60,141,188,1)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(60,141,188,1)",
        data : [3.0 , 2.7 , 3.1 , 3.1, 3.32 , 3.0 , 3.1]
      }
      ]
    };

    var areaChartOptions = {
      //Boolean - If we should show the scale at all
      showScale: true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines: false,
      //String - Colour of the grid lines
      scaleGridLineColor: "rgba(0,0,0,.05)",
      //Number - Width of the grid lines
      scaleGridLineWidth: 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines: true,
      //Boolean - Whether the line is curved between points
      bezierCurve: true,
      //Number - Tension of the bezier curve between points
      bezierCurveTension: 0.3,
      //Boolean - Whether to show a dot for each point
      pointDot: false,
      //Number - Radius of each point dot in pixels
      pointDotRadius: 4,
      //Number - Pixel width of point dot stroke
      pointDotStrokeWidth: 1,
      //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
      pointHitDetectionRadius: 20,
      //Boolean - Whether to show a stroke for datasets
      datasetStroke: true,
      //Number - Pixel width of dataset stroke
      datasetStrokeWidth: 2,
      //Boolean - Whether to fill the dataset with a color
      datasetFill: true,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
      //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio: true,
      //Boolean - whether to make the chart responsive to window resizing
      responsive: true
    };

    //Create the line chart
    areaChart2.Line(areaChartData, areaChartOptions);


  });
</script>
</body>
</html>
